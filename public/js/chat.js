const socket = io();

//elements
const $form = document.querySelector("form");
const $messageInput = $form.querySelector("#message");
const $messageButton = $form.querySelector("button");
const $locationButton = document.querySelector("#send-location");
const $messages = document.querySelector("#messages");
const $sidebar = document.querySelector("#sidebar");

//templates
const mesageTemplate = document.querySelector("#message-template").innerHTML;
const locationTemplate = document.querySelector("#location-template").innerHTML;
const sidebarTemplate = document.querySelector("#sidebar-template").innerHTML;

//Options
const { username, room } = Qs.parse(location.search, {
  ignoreQueryPrefix: true,
});

const autoscroll = () => {
  // New message element
  const $newMessage = $messages.lastElementChild;

  // Height of the new message
  const newMessageStyles = getComputedStyle($newMessage);
  const newMessageMargin = parseInt(newMessageStyles.marginBottom);
  const newMessageHeight = $newMessage.offsetHeight + newMessageMargin;

  // Visible height
  const visibleHeight = $messages.offsetHeight;

  // Height of messages container
  const containerHeight = $messages.scrollHeight;

  // How far have I scrolled?
  const scrollOffset = $messages.scrollTop + visibleHeight;

  if (containerHeight - newMessageHeight <= scrollOffset) {
    $messages.scrollTop = $messages.scrollHeight;
  } else {
    const Toast = Swal.fire({
      toast: true,
      html: `<b>You have a New Messages!</b>`,
      position: "top-right",
      iconColor: "white",
      text: "You have New Messages!",
      customClass: {
        popup: "colored-toast",
      },
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
    });
  }
};

socket.on("message", (data) => {
  // console.log(data);
  const html = Mustache.render(mesageTemplate, {
    message: data.text,
    createdAt: moment(data.createdAt).format("h:mm a"),
    username: data.username,
  });
  $messages.insertAdjacentHTML("beforeend", html);
  autoscroll();
});

socket.on("locationMessage", (location) => {
  // console.log(location);
  const html = Mustache.render(locationTemplate, {
    location: location.text,
    createdAt: moment(location.createdAt).format("h:mm a"),
    username: location.username,
  });
  $messages.insertAdjacentHTML("beforeend", html);
  autoscroll();
});

socket.on("roomData", ({ users, room }) => {
  // console.log(users);
  const html = Mustache.render(sidebarTemplate, {
    room,
    users,
  });
  $sidebar.innerHTML = html;
});

//rendering html

$form.addEventListener("submit", (e) => {
  e.preventDefault();

  const msg = e.target.elements.message.value;
  if (!msg || msg.length < 0) {
    return;
  }

  $messageButton.setAttribute("disabled", "disabled");
  $messageInput.setAttribute("disabled", "disabled");

  socket.emit("sendMessage", msg, (err) => {
    $messageButton.removeAttribute("disabled");
    $messageInput.removeAttribute("disabled");
    $messageInput.value = "";
    $messageInput.focus();

    if (err) {
      return console.log(err);
    }
    // console.log(`message was delivered successfully`);
  });
});

$locationButton.addEventListener("click", () => {
  if (!navigator.geolocation) {
    return alert(" Geolocation is not supported in your browser");
  }

  $locationButton.setAttribute("disabled", "disabled");

  navigator.geolocation.getCurrentPosition((position) => {
    const { latitude, longitude } = position.coords;

    //emitting location to others

    socket.emit("sendLocation", { latitude, longitude }, () => {
      $locationButton.removeAttribute("disabled");
      console.log("location shared!");
    });
  });
});

socket.emit("join", { username, room }, (err) => {
  if (err) {
    alert("Error : " + err);
    location.href = "/";
  }
});
