const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const path = require("path");
const http = require("http");
const { Server } = require("socket.io");
const Filter = require("bad-words");
const { generateMessage, generateLocationMessage } = require("./utils/message");
const {
  addUser,
  getUser,
  removeUser,
  getUsersInRoom,
} = require("./utils/users");

const app = express();
const httpServer = http.createServer(app);
const io = new Server(httpServer);

const PORT = process.env.PORT || 8080;
const staticDirectoryPath = path.join(__dirname, "../public");

app.use(express.json());
app.use(cors());
app.use(helmet());
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      "script-src": [
        "'self'",
        "https://cdnjs.cloudflare.com",
        "https://cdn.jsdelivr.net",
      ],
    },
  })
);

app.use(express.static(staticDirectoryPath));

io.on("connection", (socket) => {
  //socket.emit, io.emit, socket.broadcast.emit -- general
  // io.to.emit, socket.broadcast.to.emit -- room specific

  //create join room for given group name
  socket.on("join", ({ username, room }, callback) => {
    const { error, user } = addUser(socket.id, username, room);

    if (error) {
      return callback(error);
    }

    socket.join(user.room);

    //emit to only connected user
    socket.emit("message", generateMessage("Welcome!"));
    //broadcast message to all other connected users but not to this user
    socket.broadcast
      .to(room)
      .emit("message", generateMessage(`${user.username} has joined`));

    io.to(user.room).emit("roomData", {
      room: user.room,
      users: getUsersInRoom(user.room),
    });
    callback();
  });

  //callback for sending acknowledgement
  socket.on("sendMessage", (msg, callback) => {
    const filter = new Filter();

    if (filter.isProfane(msg)) {
      return callback("profanity is not allowed!");
    }

    const user = getUser(socket.id);

    if (user) {
      io.to(user.room).emit("message", generateMessage(msg, user.username));
    }
    callback();
  });

  socket.on("sendLocation", (coords, callback) => {
    const user = getUser(socket.id);
    if (user) {
      io.to(user.room).emit(
        "locationMessage",
        generateLocationMessage(coords, user.username)
      );
    }
    callback();
  });

  socket.on("disconnect", () => {
    const user = removeUser(socket.id);
    if (user) {
      io.to(user.room).emit(
        "message",
        generateMessage(`${user.username} has left!`)
      );

      io.to(user.room).emit("roomData", {
        room: user.room,
        users: getUsersInRoom(user.room),
      });
    }
  });
});

httpServer.listen(PORT, () => {
  console.log("server is up and running on port " + PORT);
});
