const generateMessage = (msg, username = "Admin") => {
  return {
    text: msg,
    createdAt: new Date().getTime(),
    username,
  };
};

const generateLocationMessage = ({ latitude, longitude }, username) => {
  return {
    text: `https://google.com/maps?q=${latitude},${longitude}`,
    createdAt: new Date().getTime(),
    username,
  };
};

module.exports = { generateMessage, generateLocationMessage };
