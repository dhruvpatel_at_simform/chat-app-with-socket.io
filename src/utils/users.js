const users = [];

// addUser, removeUser, getUser, getUserInRoom

const addUser = (id, username, room) => {
  //clean the data
  username = username.trim().toLowerCase();
  room = room.trim().toLowerCase();

  //Validate the data
  if (!username || !room) {
    return {
      error: "Username and room must be provided",
    };
  }

  //check for existing username
  const isExistingUser = users.find(
    (user) => user.room === room && user.username === username
  );

  if (isExistingUser) {
    return {
      error: "Username is in use",
    };
  }

  //store user
  const user = { id, username, room };
  users.push(user);
  return { user };
};

//remove the user

const removeUser = (id) => {
  const index = users.findIndex((user) => user.id === id);

  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
};

//get User from users

const getUser = (id) => {
  const user = users.find((user) => user.id === id);
  return user;
};

//get users who belongs to same room
const getUsersInRoom = (room) => {
  const usersInRoom = users.filter((user) => user.room === room);
  return usersInRoom;
};

module.exports = {
  addUser,
  removeUser,
  getUsersInRoom,
  getUser,
};

// addUser({
//   id: "abcd",
//   username: "Dj",
//   room: "my-room",
// });

// addUser({
//   id: "efgh",
//   username: "Pj",
//   room: "my-room",
// });

// addUser({
//   id: "ijkl",
//   username: "Mj",
//   room: "my-room2",
// });

// console.log(getUser("fgfg"));

// console.log(getUsersInRoom("my-room3"));
